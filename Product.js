import React, { Component } from 'react';
import {
  Col, Button, DropdownButton, MenuItem,
  Alert, Image, Tabs, Tab,
  Modal, FormControl,
} from 'react-bootstrap';
import { browserHistory, Link, } from 'react-router';
import firebase from 'firebase';
import slug from 'slug';
import ReactQuill from "react-quill";
import Dropzone from 'react-dropzone';

/** COMPONENTS CUSTOM **/
import { APP_CONFIG } from './../../../boot.config';
import { TOOLS } from './../../../util/tools.global';
import { MasterForm } from './../../Global/Form/MasterForm';
// import { Input } from '../../Form/Input';
export class Product extends Component{
  constructor(props){
    super(props);
    this.state = {
      appID: document.querySelector('[data-js="root"]').dataset.appid || null,

      isLoading : true,
      showModal : false,
      confirmDelete : false,
      action : this.props.action,
      products : [],
      categories : [],
      files: [],
      previews: [],

      uid : this.props.currentUser.uid,
      productID : null,
      title : null,
      slug : null,
      description : '',
      shortDescription : '',
      typeContent : 'Product',
      category : false,
      qtd : 0,
      price : 0.0,
      photos: [],
      photoPrimary: null,
      status : true,

      status_response : null,
      message_response : null
    }

    this.checkStatusResponse = this.checkStatusResponse.bind(this);
    this.checkStates = this.checkStates.bind(this);
    this.getProducts = this.getProducts.bind(this);
    this.resetCurrentState = this.resetCurrentState.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.requiredInput = TOOLS.requiredInput.bind(this);

    /*** FUNCTIONS AUXILIARES DO FORM ***/
    this.onLoadForm = this.onLoadForm.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onUpdateForm = this.onUpdateForm.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);
    this._onChangeTitle = this._onChangeTitle.bind(this);
    this._onChangePrice = this._onChangePrice.bind(this);
    this._onChangeQtd = this._onChangeQtd.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeShortDescription = this.onChangeShortDescription.bind(this);
    this.renderType = this.renderType.bind(this);
    this.onSelectCategory = this.onSelectCategory.bind(this);
    this.statusInfo = this.statusInfo.bind(this);

    this.onDropFile = this.onDropFile.bind(this);
    this.onDropFileClick = this.onDropFileClick.bind(this);
    this.setPhotoPrimary = this.setPhotoPrimary.bind(this);
    this.runUploadFiles = this.runUploadFiles.bind(this);

    /*** RENDER FUNCTIONS - TABS **/
    this.tabGeral = this.tabGeral.bind(this);
    this.tabImagens = this.tabImagens.bind(this);
    this.tabAtribuirModulos = this.tabAtribuirModulos.bind(this);

    /*** RENDER FUNCTIONS - ACTIONS ***/
    this.actionNull = this.actionNull.bind(this);
    this.actionNew = this.actionNew.bind(this);
    this.actionDelete = this.actionDelete.bind(this);
    this.renderAction = this.renderAction.bind(this);
  }

  componentWillMount(){
    // this.getProducts();
    console.log('componentWillMount');
    if(this.props.action === 'edit' && this.props.productID){
      console.log('componentWillMount if edit ')
      this.setState({ productID : this.props.productID });
    }
  }

  componentDidMount(){
    console.log('User id -> ',this.state.uid);
    let _self=this;
    _self.allProducts = firebase.database().ref('apps/'+ this.state.appID +'/products/');
    _self.allCategories = firebase.database().ref('apps/'+ this.state.appID +'/categories/');
    _self.products = this.state.products;
    _self.categories = this.state.categories;

    /** child_added **/
    _self.allProducts.on('child_added', (data) => {
      _self.products = this.state.products;
      _self.products.push(data.val());
      if(data){
        if(_self.products.length >= 0 ){
          _self.setState({ products : _self.products, isLoading : false });
        }
      }else{
        _self.setState({ isLoading : false });
      }
    });
    _self.allCategories.on('child_added', (data) => {
      _self.categories.push(data.val());
      if(data){
        if(_self.categories.length > 0 )
          _self.setState({ categories : _self.categories, isLoading : false });
      }else{
        _self.setState({ isLoading : false });
      }
    });
    /** child_added **/

    /** child_changed **/
    _self.allProducts.on('child_changed', (data) => {
      if(data){
        let changed = data.val();
        if(_self.products.length > 0 ){
          console.log('child_changed products: ', {changed : changed});
          // eslint-disable-next-line
          _self.products.map((product) => {
            if(product.productID === changed.productID){
              product.uid = this.state.uid;
              product.title = changed.title;
              product.slug = changed.slug;
              product.description = changed.description;
              product.shortDescription = changed.shortDescription;
              product.category = changed.category;
              product.typeContent = changed.typeContent;
              product.qtd = changed.qtd;
              product.price = changed.price;
              product.status = changed.status;
            }
          });
          _self.setState({ isLoading : false });
        }

        _self.setState({ products : _self.products });
      }else{
        _self.setState({ isLoading : false });
      }
    });
    _self.allCategories.on('child_changed', (data) => {
      if(data){
        let changed = data.val();
        if(_self.allCategories.length > 0 ){
          console.log('child_changed allCategories: ', {changed : changed});
          // eslint-disable-next-line
          _self.allCategories.map((category) => {
            if(category.categoryID === changed.categoryID){
              category.uid = this.state.uid;
              category.title = changed.title;
              category.description = changed.description;
              category.status = changed.status;
            }
          });
          _self.setState({ isLoading : false });
        }

        _self.setState({ allCategories : _self.allCategories });
      }else{
        _self.setState({ isLoading : false });
      }
    });
    /** child_changed **/

    /** child_moved **/
    _self.allProducts.on('child_moved', (data) => {
      _self.products.push(data.val());
      if(data){
        if(_self.products.length > 0 )
          _self.setState({ isLoading : false });

        _self.setState({ products : _self.products });
      }else{
        _self.setState({ isLoading : false });
      }
      console.log('child_moved products: ', {qtd: _self.products.length, all: _self.products});
    });
    _self.allCategories.on('child_moved', (data) => {
      _self.allCategories.push(data.val());
      if(data){
        if(_self.allCategories.length > 0 )
          _self.setState({ isLoading : false });

        _self.setState({ allCategories : _self.allCategories });
      }else{
        _self.setState({ isLoading : false });
      }
      console.log('child_moved allCategories: ', {qtd: _self.allCategories.length, all: _self.allCategories});
    });
    /** child_moved **/

    /** child_removed **/
    _self.allProducts.on('child_removed', (data) => {
      if(data){
        if(_self.products.length > 0 ){
          let productRemoved = data.val();
          console.log('res child_removed -> ',productRemoved);
          // eslint-disable-next-line
          _self.products.map((product, key) => {
            // eslint-disable-next-line
            if(product.productID === productRemoved.productID){
              console.log('Key -> ',key);
              TOOLS.removeArrayItem(_self.products,key,1);
              console.log('Product removeed ',productRemoved.productID);
              _self.setState({ products : _self.products, isLoading : false });
            }else{
              console.log('Key -> ',key);
              console.log('Product productID ',product.productID);
              console.log('Product removeed ',productRemoved.productID);
            }
          });
        }
      }else{
        _self.setState({ isLoading : false });
      }
    });
    _self.allCategories.on('child_removed', (data) => {
      if(data){
        if(_self.categories.length > 0 ){
          let categoryRemoved = data.val();
          console.log('res child_removed -> ',categoryRemoved);
          // eslint-disable-next-line
          _self.categories.map((category, key) => {
            // eslint-disable-next-line
            if(category.categoryID === categoryRemoved.categoryID){
              console.log('Key -> ',key);
              TOOLS.removeArrayItem(key,1);
              console.log('Category removeed ',categoryRemoved.categoryID);
              _self.setState({ categories : _self.categories, isLoading : false });
            }else{
              console.log('Key -> ',key);
              console.log('Category categoryID ',category.categoryID);
              console.log('Category removeed ',categoryRemoved.categoryID);
            }
          });
        }
      }else{
        _self.setState({ isLoading : false });
      }
    });
    /** child_removed **/
    _self.setState({ isLoading : false });
  };//componentDidMount();

  componentWillReceiveProps(nextProps){
    console.log('componentWillReceiveProps ',nextProps);

    if(nextProps.action === 'new'){
      return this.resetCurrentState();
    }else if(nextProps.categoryID || nextProps.action === 'edit'){
      console.log('getProducts to ',nextProps.action);
      console.log('getProducts to ',nextProps);
      return this.getProducts(nextProps.productID);
    }
  };//componentWillReceiveProps();

  checkStatusResponse(){
    const closeAlert = (e) => {
      if(e)
        e.preventDefault();
      this.setState({ status_response : null, message_response : null});
    };//closeAlert(e);
    if(this.state.status_response === true){
      setTimeout(() => {
        closeAlert();
      },4000);
      return (<Alert bsStyle={'success'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    if(this.state.status_response === false){
      setTimeout(() => {
        closeAlert();
      },4000);
      if(this.state.status === 'Rascunho'){
        return (<Alert className={'default'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }else if(this.state.status === 'requiredInput'){
        return (<Alert className={'warning'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }
      return (<Alert bsStyle={'danger'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    return null;
  };//checkStatusResponse();

  checkStates(){
    if(this.state.productID)
      this.setState({ productID : this.state.productID })

    console.log('checkStates exit();');
  };//checkStates();

  getProducts(productID){
    // console.log('Init getProducts ->',productID);
    if( productID ){
      // console.log('Getting productID ->',productID);
      // eslint-disable-next-line
      return this.state.products.map((product) => {
        // eslint-disable-next-line
        if(product.productID === productID){
          this.setState({
            productID : product.productID,
            title : product.title,
            slug : product.slug,
            description : product.description,
            shortDescription : product.shortDescription,
            typeContent : product.typeContent,
            category : product.category,
            qtd : product.qtd,
            price : product.price,
            photos : product.photos,
            photoPrimary : product.photoPrimary,
            status: product.status
          });
        }else{
          return false;
        }
      });
    }
    this.setState({ isLoading : false });
  };//getProducts();

  resetCurrentState(){
    console.log('resetCurrentState();');
    return this.setState({
      showModal : false,
      confirmDelete : false,
      action : this.props.action,

      uid : this.props.currentUser.uid,
      productID : TOOLS.uniqueID(),
      title : null,
      slug : null,
      description : null,
      shortDescription : null,
      typeContent : 'Product',
      category : 'Selecione uma categoria',
      qtd : 0,
      price : 0.0,
      photos : [],
      photoPrimary : null,
      previews : [],
      files : [],
      status : true,

      status_response : null,
      message_response : null
    });
  };//resetCurrentState();

  renderModal(){
    let
      productTitle = this.state.title;
    const close = () => {
      this.setState({ showModal : false });
    }
    const confirm = () => {
      firebase.database().ref('apps/'+ this.state.appID +'/products/'+this.state.productID)
        .remove()
        .then(() => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : false,
            status : 'Deletado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } deletado com sucesso!'
          });
        })
        .catch((err) => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : true,
            status : 'Error',
            status_response : false,
            message_response : 'Ocorreu uma falha ao tentar deletar { ' + this.state.title + ' } - '+err.message
          });
        });
    }
    return (
      <Modal show={this.state.showModal} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>Deseja realmente excluir {productTitle}?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Link to={'/dashboard/products'} onClick={close.bind(this)} className={'btn btn-danger'}>Cancelar</Link>
          <Button onClick={confirm.bind(this)} bsStyle={'success'}>Confirmar</Button>
        </Modal.Footer>
      </Modal>
    );
  };//renderModal();

  /*** FUNCTIONS AUXILIARES DO FORM ***/
  onLoadForm(){
    console.log('onLoadForm();');
  };//onLoadForm(e);
  onSubmitForm(e) {
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const productID = this.state.productID;
      const dataInsert = {
        uid : this.state.uid,
        productID : productID,
        title : this.state.title,
        slug : this.state.slug.toLowerCase(),
        description : this.state.description,
        shortDescription : this.state.shortDescription,
        typeContent : this.state.typeContent,
        category : this.state.category,
        qtd : this.state.qtd,
        price : this.state.price,
        photos : this.state.photos,
        photoPrimary: this.state.photoPrimary,
        status: 'Publicado'
      };
      firebase.database().ref('apps/'+ this.state.appID +'/products/'+productID)
        .set(dataInsert, () => {
          this.setState({
            isLoading : false,
            status_response : true,
            message_response : '{ ' + dataInsert.title + ' } gravado com sucesso!'
          });
          console.clear();
          browserHistory.push('/dashboard/products');
        });
    }
    return false;
  };//onSubmitForm(e);
  onUpdateForm(e){
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const productID = this.state.productID;
      const dataUpdate = {
        uid : this.state.uid,
        productID : productID,
        title : this.state.title,
        slug : this.state.slug.toLowerCase(),
        description : this.state.description,
        shortDescription : this.state.shortDescription,
        typeContent : this.state.typeContent,
        category : this.state.category,
        qtd : this.state.qtd,
        price : this.state.price,
        photos : this.state.photos,
        status: this.state.status
      };
      console.log('dataUpdate -> ',dataUpdate);
      firebase.database().ref('apps/'+ this.state.appID +'/products/'+productID)
        .update(dataUpdate, () => {
          this.setState({
            isLoading : false,
            productID : productID,
            description: '',
            status : 'Publicado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
          });
          console.log('Update successful ',this.state.products)
          browserHistory.push('/dashboard/products');
        });
    }
    return false;
  };//onUpdateForm(e);
  onChangeForm(e){
    console.log('onChangeForm',e)
    if(e){
      // eslint-disable-next-line
      this.state.products.map((product) => {
        if(product.slug === slug(e.target.value)){
          this.setState({
            productID : this.state.productID,
            title : this.refs.title.value,
            slug : slug(this.refs.title.value)+'_2',
            description : this.refs.description.value,
            shortDescription : this.refs.shortDescription.value,
            typeContent: this.state.typeContent,
            category: this.state.category,
            qtd : this.refs.qtd.value,
            price : this.state.price,
            photos : this.state.photos,
            status: this.state.status
          });
        }else{
          this.setState({
            productID : this.refs.productID.value,
            title : this.refs.title.value,
            slug : slug(this.refs.title.value),
            description : this.refs.description.value,
            shortDescription : this.refs.shortDescription.value,
            typeContent: this.state.typeContent,
            category: this.state.category,
            qtd : this.refs.qtd.value,
            price : this.state.price,
            photos : this.state.photos,
            status: this.state.status
          });
        }
      });
    }
  };//onChangeForm(e);
  _onChangeTitle(e){
    if(e){
      this.setState({title: e.target.value, slug: slug(e.target.value), status : 'Publicado'});
    }
  };//_onChangeTitle(e);
  _onChangePrice(e){
    if(e){
      e.target.value = TOOLS.formatReal(TOOLS.getMoney(e.target.value));
      this.setState({price : TOOLS.formatReal(TOOLS.getMoney(e.target.value))});
    }
  };//_onChangePrice(e);
  _onChangeQtd(e){
    if(e){
      e.target.value = Number(e.target.value);
      this.setState({qtd : Number(e.target.value)});
    }
  };//_onChangeQtd(e);
  onChangeDescription(value){
      this.setState({ description : value });
  };//onChangeDescription(e);
  onChangeShortDescription(value){
      this.setState({ shortDescription : value });
  };//onChangeShortDescription(e);
  renderType(){
    if(this.state.typeContent === 'Product'){
      const renderDropdownCategories = () => {
        if(this.state.categories.length === 0){

        }else{
          return this.state.categories.map((category) => {
            return (
              <MenuItem key={category.categoryID} className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectCategory}>{category.title}</MenuItem>
            );
          });
        }
      }
      return (
        <div>
          <label>Categoria</label>
          { (this.state.categories.length > 0) ?
            <DropdownButton id={'categoryList'} className={'col-xs-12 col-md-12'} title={this.state.category}>
              {renderDropdownCategories()}
            </DropdownButton>
          :
          <Link to={'/dashboard/categories/new'} className={'categoryList btn-sm btn-warning text-center no-radius col-xs-12 col-md-12'} title={'clique e cadastre uma categoria'}>Nenhuma categoria cadastrada</Link>
          }
        </div>
      );
    }else{
      return (
        <div>
          <label>Tipo de Conteúdo</label>
          <DropdownButton id={'typeContent'} className={'col-xs-12 col-md-12'} title={this.state.typeContent}>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Product</MenuItem>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Post</MenuItem>
          </DropdownButton>
        </div>
      );
    }
  };//renderType();
  onSelectCategory(e){
    if(e && e.target.innerHTML !== '')
      this.setState({ category : e.target.innerHTML });
  };//onSelectCategory();
  statusInfo(){
    if( this.state.status ){
      if(this.state.status === 'Publicado')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-success' }>{'Publicado'}</span>
          </label>
        );

      if(this.state.status === 'Rascunho')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{this.state.status || 'Aguardando publicação..'}</span>
          </label>
        );

      if(this.state.status === 'requiredInput')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{'Rascunho'}</span>
          </label>
        );
    }else{
      return (
        <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
          <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{'Aguardando um título'}</span>
        </label>
      );
    }
  };//statusInfo();

  onDropFile(files){
    console.log('Received files: ', files);
    this.setState({files:files});
    this.runUploadFiles(files);
  };//onDropFile(files);

  onDropFileClick(){
    console.log('this.refs.dropzone' , this.refs.dropzone)
    this.refs.dropzone.open();
  };//onDropFileClick();

  setPhotoPrimary(e){
    e.preventDefault();
    if(this.state.photoPrimary){
      let selected = document.querySelector('li.selected');
      selected.classList.remove('selected');
      e.target.parentElement.parentElement.classList.toggle('selected');
    }else{
      e.target.parentElement.parentElement.classList.toggle('selected');
    }
    this.setState({photoPrimary : e.target.src});
  };//setPhotoPrimary(e);

  runUploadFiles(files){
    console.log('runUploadFiles');
    let storageRef = firebase.storage().ref('apps/'+ this.state.appID +'/products/'+this.state.productID+'/'),
        collectionPhotos = this.state.photos,
        collectionPreviews = this.state.previews;

    if(files.length > 0){
      // eslint-disable-next-line
      files.map((file,key) => {
        let fileID = TOOLS.uniqueID(),
            fileName_old = file.name,
            fileName = fileID + '_' + fileName_old,
            fileRef = storageRef.child('images/' + fileName),
            // pathFile = fileRef.fullPath,
            // fileSize = file.size,
            // fileType = file.type,
            uploadTask = fileRef.put(file);
            this.setState({isLoading : true});

        uploadTask.on('state_changed', (snapshot) => {
          console.log('snapshot -> ',snapshot);
        }, (error) => {
          console.log('error -> ',error);
        }, () => {
          fileRef.getDownloadURL().then((url) => {
            collectionPhotos.push(url);
            collectionPreviews.push(file.preview);
            this.setState({
              photos:collectionPhotos,
              previews:collectionPreviews
            });
            if(this.state.photos.length === files.length){
              console.log('if this.state.photos.length -> ',this.state.photos.length);
              this.setState({isLoading:false});
            }else{
              console.log('this.state.photos.length < -> ',this.state.photos.length);
            }
          }).catch((error) => {
            console.log(error);
          });
        });

        console.log('collectionPhotos -> ', collectionPhotos);
        console.log('uploadTask -> ', uploadTask);
      });
    }
  };//runUploadFiles();


  tabGeral(){
    return (
      <MasterForm onLoadForm={this.onLoadForm} onChange={this.onChangeForm} onSubmit={this.onSubmitForm}>
        <Col xs={12} md={8} className={'no-padding'}>
          <div className={'input-group col-md-12'}>
            <label>Título</label>
              <input
                type={'hidden'}
                ref={'status'}
                value={'Publicado'}
                />
            <input
              className={'form-control required'}
              onBlur={TOOLS.requiredInput}
              onChange={this._onChangeTitle}
              type="text"
              ref="title"
              autoFocus
              placeholder="Digite o título aqui" />
              <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
              </label>
          </div>

          <div className={'input-group col-md-12'} style={{marginTop:20}}>
            <ReactQuill
              theme={'snow'}
              ref={'description'}
              value={this.state.description}
              onChange={this.onChangeDescription}
              />
          </div>
          <div className={'input-group col-md-12'} style={{marginTop:20}}>
            <label>Descrição Curta</label>
            <ReactQuill
              theme={'snow'}
              ref={'shortDescription'}
              value={this.state.shortDescription}
              onChange={this.onChangeShortDescription}
              />
          </div>
        </Col>
        <Col xs={12} md={4} className={'no-paddingRight'}>
          {this.statusInfo()}
          {this.renderType()}
          <div className={'input-group col-md-12'} style={{marginTop:20}}>
            <div className={'no-padding col-md-7 pull-left'}>
              <label>Preço</label>
              <div className={'input-group'}>
                <span className={'no-radius input-group-addon'}>R$</span>
                <input
                  className={'form-control no-radius text-center'}
                  onBlur={TOOLS.requiredInput}
                  onChange={this._onChangePrice}
                  type={'text'}
                  ref={'price'}
                />
              </div>
            </div>
            <div className={'input-group col-md-4 pull-right'}>
              <label>Quantidade</label>
              <input
                className={'form-control no-radius text-center'}
                onBlur={TOOLS.requiredInput}
                onChange={this._onChangeQtd}
                type={'number'}
                ref={'qtd'}
              />
            </div>
          </div>
          <div className={'input-group col-md-12'} style={{marginTop:20}}>
            <label>Imagem Principal</label>
            { this.state.photoPrimary ?
              <figure><Image src={this.state.photoPrimary} responsive width={'100%'} /></figure>
            :
              <figure><Image src={'http://89imoveis.com/images/naodisponivel.png'} responsive width={'100%'} /></figure>
            }
          </div>
          <Button
            onClick={this.onSubmitForm}
            className="btn btn-success pull-right no-radius"
            style={{marginTop:30}}
            >Publicar
          </Button>
          <Link
            to={'/dashboard/products/'}
            className="btn btn-danger pull-right no-radius"
            style={{marginTop:30}}
            >Cancelar
          </Link>
        </Col>
      </MasterForm>
    );
  }//tabGeral();

  tabImagens(){
    return (
      <div className={'input-group col-md-12'}>
        <Dropzone ref="dropzone" className={'dropzone'} activeClassName={'dragIn'} onDrop={this.onDropFile} >
          <span>Arraste suas fotos para cá, ou clique para selecionar os arquivos para upload.</span>
        </Dropzone>
        <button type="button" onClick={this.onDropFileClick} style={{display:'none'}}>
          OpenBox
        </button>
        {this.state.files ?
          <div className={'filesUploaded'}>
            <h2>Arquivos <span className="badge">{this.state.files.length}</span> {!this.state.photoPrimary ? <span className={'label label-info pull-right'} style={{fontSize:'14px',lineHeight:'16px'}}>Por favor, selecione a imagem principal do seu produto.</span> : ''}</h2>
            <div>
              <ul>{
                // eslint-disable-next-line
                this.state.photos.map((url,key) => <li onClick={this.setPhotoPrimary} key={key}><figure><Image src={url} responsive /></figure></li>)
              }
              </ul>
            </div>
          </div>
        :
          <div className={'filesUploaded'}>
            <h2>Arquivos <span className="badge">{this.state.previews.length}</span></h2>
            <div className={'empty'}><span>{this.state.previews.length} arquivos</span></div>
          </div>
        }
      </div>
    );
  }//tabImagens();

  tabAtribuirModulos(){
    return (
      <Col xs={12} md={12} className={'no-padding'}>

      </Col>
    );
  }//tabAtribuirModulos();

  /*
   *	 RENDER FUNCTIONS - ACTIONS
   *
   *  Funções que renderizam a view do component,
   *  de acordo a ACTION de this.props.action;
   *  Todas as "ACTIONS Functions" definem document.title:
   *  Ex: document.title = APP_CONFIG.PROJECT_NAME + ' | Product Name';
   *  { actionNull - actionNew - actionEdit - actionDelete }
   */
  actionNull(){
    /*
     *  Responsável por renderizar a table de Páginas
     *  if( this.state.products.length > 0 )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Produtos';
    if( this.state.products.length > 0 ){
      const renderList = () => {
        return this.state.products.map((product) => {
          return (
            <tr key={product.productID}>
              <td style={{width:80}}>
                <Link to={'/dashboard/products/edit/'+product.productID} className={'btn btn-xs btn-primary'}>
                  <span className={'fa fa-pencil'}></span>
                </Link>
                <Link to={'/dashboard/products/delete/'+product.productID} onClick={() => {this.setState({showModal:true,title:product.title,productID:product.productID})}} className={'btn btn-xs btn-danger'}>
                  <span className={'fa fa-trash'}></span>
                </Link>
              </td>
              <td style={{width:60}}><small>{product.productID}</small></td>
              <td><small>{product.title}</small></td>
              <td><small>{TOOLS.stripAllTags(product.description).slice(0,59)+'..'}</small></td>
              <td><small>{product.category ? product.category : 'SEM CATEGORIA'}</small></td>
              <td><small>R$ {product.price}</small></td>
              <td className={'text-center'}><small>{product.qtd}</small></td>
              <td className={'text-center'}><small>{product.status}</small></td>
            </tr>
          );
        });
      }

      return (
        <div className={'table-responsive'}>
          <table className={'table table-hover'}>
            <thead>
              <tr>
                <th></th>
                <th>#ID</th>
                <th>TÍTULO</th>
                <th>DESCRIPTION</th>
                <th>CATEGORIA</th>
                <th>PREÇO</th>
                <th className={'text-center'}>QTD</th>
                <th className={'text-center'}>STATUS</th>
              </tr>
            </thead>
            <tbody>
              { renderList() }
            </tbody>
          </table>
        </div>
      );
    }else{
      return (
        <div className={'table-responsive'}>
          <table className={'table table-hover'}>
            <thead>
              <tr>
                <th>ID</th>
                <th>TÍTULO</th>
                <th>TIPO</th>
                <th>CATEGORIA</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td className={'text-center'}>{'Nenhum registro foi encontrado... '}</td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  };//actionNull();

  actionNew(){
    /*
     *  Responsável por renderizar o Form para inserção de Produtos
     *  if( this.props.action === 'new' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Adicionar Produto';

    return (
      <div>
        <Col xs={12} md={12} className={'no-padding'}>
          <Tabs defaultActiveKey={1} id="productsTab">
            <Tab eventKey={1} title="Geral">
              {this.tabGeral()}
            </Tab>
            <Tab eventKey={2} title="Imagens">
              {this.tabImagens()}
            </Tab>
            <Tab eventKey={3} title="Atribuir à modulo">
              {this.tabAtribuirModulos()}
            </Tab>
          </Tabs>
        </Col>
      </div>
    );
  };//actionNew();

  actionEdit(){
    /*
     *  Responsável por renderizar o Form para alteração de Produtos
     *  if( this.props.action === 'edit' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Editar Produto';

    if(this.props.action === 'edit'){
      console.log('action: ',this.props.action);
      if(this.props.productID){
        return (
          <div>
            <Col xs={12} md={12} className={'no-padding'}>
              <Tabs defaultActiveKey={1} id="products">
                <Tab eventKey={1} title="Geral">
                  <MasterForm onLoadForm={this.onLoadForm} onChange={this.onChangeForm} onSubmit={this.onUpdateForm} style={{marginTop:20}}>
                    <Col xs={12} md={8} className={'no-padding'}>
                      <div className={'input-group col-md-12'}>
                        <label>Título</label>
                        <FormControl
                          type={'hidden'}
                          ref={'productID'}
                          value={this.props.productID}
                          />
                        <FormControl
                          className={'form-control'}
                          onBlur={TOOLS.requiredInput}
                          onChange={this._onChangeTitle}
                          type="text"
                          ref={'title'}
                          value={this.state.title}
                          placeholder="Digite o título aqui" />
                          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                          </label>
                      </div>

                      <div className={'input-group col-md-12'} style={{marginTop:20}}>
                        <ReactQuill
                          theme={'snow'}
                          ref={'description'}
                          value={this.state.description}
                          onChange={this.onChangeDescription}
                          />
                      </div>
                      <div className={'input-group col-md-12'} style={{marginTop:20}}>
                        <label>Descrição Curta</label>
                        <ReactQuill
                          theme={'snow'}
                          ref={'shortDescription'}
                          value={this.state.shortDescription}
                          onChange={this.onChangeShortDescription}
                          />
                      </div>
                    </Col>
                    <Col xs={12} md={4} className={'no-paddingRight'}>
                      {this.statusInfo()}
                      {this.renderType()}
                      <div className={'input-group col-md-12'} style={{marginTop:20}}>
                        <div className={'no-padding col-md-6 pull-left'}>
                          <label>Preço</label>
                          <div className={'input-group'}>
                            <span className={'no-radius input-group-addon'}>R$</span>
                            <input
                              className={'form-control no-radius text-center'}
                              onBlur={TOOLS.requiredInput}
                              onChange={this._onChangePrice}
                              type={'text'}
                              ref={'price'}
                              value={this.state.price}
                            />
                          </div>
                        </div>
                        <div className={'input-group col-md-4 pull-right'}>
                          <label>Quantidade</label>
                          <input
                            className={'form-control no-radius text-center'}
                            onBlur={TOOLS.requiredInput}
                            onChange={this._onChangeQtd}
                            type={'number'}
                            ref={'qtd'}
                            value={this.state.qtd}
                          />
                        </div>
                      </div>
                      <div className={'input-group col-md-12'} style={{marginTop:20}}>
                        <label>Imagem Principal</label>
                        { this.state.photoPrimary ?
                          <figure><Image src={this.state.photoPrimary} responsive width={'100%'} /></figure>
                        :
                          <figure><Image src={'http://89imoveis.com/images/naodisponivel.png'} responsive width={'100%'} /></figure>
                        }
                      </div>
                      <Button
                        onClick={this.onUpdateForm}
                        className="btn btn-success pull-right no-radius"
                        style={{marginTop:30}}
                        >Atualizar
                      </Button>
                      <Link
                        to={'/dashboard/products/'}
                        className="btn btn-danger pull-right no-radius"
                        style={{marginTop:30}}
                        >Cancelar
                      </Link>
                    </Col>
                  </MasterForm>
                </Tab>
                <Tab eventKey={2} title="Imagens">
                  <div className={'input-group col-md-12'}>
                    <Dropzone ref="dropzone" className={'dropzone'} activeClassName={'dragIn'} onDrop={this.onDropFile} >
                      <span>Arraste suas fotos para cá, ou clique para selecionar os arquivos para upload.</span>
                    </Dropzone>
                    <button type="button" onClick={this.onDropFileClick} style={{display:'none'}}>
                      OpenBox
                    </button>
                    {this.state.files ?
                      <div className={'filesUploaded'}>
                        <h2>Arquivos <span className="badge">{this.state.files.length}</span> {!this.state.photoPrimary ? <span className={'label label-info pull-right'} style={{fontSize:'14px',lineHeight:'16px'}}>Por favor, selecione a imagem principal do seu produto.</span> : ''}</h2>
                        <div>
                          <ul>{
                            // eslint-disable-next-line
                            this.state.photos.map((url,key) => <li onClick={this.setPhotoPrimary} key={key}><figure><Image src={url} responsive /></figure></li>)
                          }
                          </ul>
                        </div>
                      </div>
                    :
                      <div className={'filesUploaded'}>
                        <h2>Arquivos <span className="badge">{this.state.previews.length}</span></h2>
                        <div className={'empty'}><span>{this.state.previews.length} arquivos</span></div>
                      </div>
                    }
                  </div>
                </Tab>
              </Tabs>
            </Col>
          </div>
        );
      }else{
        return (<h1>Ops.. ocorreu uma falha..</h1>);
      }
      // console.log('productID: ',this.props.productID);
    }
  };//actionEdit();

  actionDelete(){
    /*
     *  Responsável por deletar produtos
     *  if( this.props.action === 'delete' )
     */
     if(this.props.action === 'delete'){
       if(this.props.productID){
         if(!this.state.confirmDelete){
           return (
             <div>
               {this.actionNull()}
               {this.renderModal()}
             </div>
           );
         }else{
           console.log('confirmDelete!');
           return (
             <div>
               {this.actionNull()}
             </div>
           );
         }
       }else{
         return (
           <h1>actionDelete #OFF</h1>
         );
       }
     }
  };//actionDelete();

  renderAction(){
    /*
     *  Responsável por verificar e chamar ACTIONS.
     *  Ex: if( this.props.action === 'new' ) this.actionNew();
     */
    if( this.props.action === 'new' )
      return this.actionNew();

    if(this.props.action === 'edit')
      return this.actionEdit();

    if(this.props.action === 'delete')
      return this.actionDelete();


    if( !this.props.action )
      return (<div>{this.actionNull()}</div>);
  };//renderAction();

  render(){
    return (
      <div>
        { this.state.isLoading ?
          <div className={'loadingOverlay'}>
            <div className="spinner white large">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>
          </div>
        :
          <div className={'loadingOverlay hidden'}>
            <div className="spinner white large">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>
          </div>
        }
        {this.checkStatusResponse()}
        {
          this.renderAction()
        }
      </div>
    );
  }
}
